#include <iostream>
#include "list.h"
using namespace std;

template<class T>
List<T>::~List() {
	for (Node<T> *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

template<class T>
void List<T>::pushToHead(T el)
{
	head = new Node<T>(el, head, 0);
	if (tail == 0)
	{
		tail = head;
	}
	else
	{
		head->next->prev = head;
	}
}

template<class T>
void List<T>::pushToTail(T el)
{
	//TO DO!
	Node<T> *newNode = new Node<T>(el, head);
	if (head == NULL) {
		head = newNode;
		tail = newNode;
	}
	else {
		tail->next = newNode;
		tail = newNode;
	}
}

template<class T>
T List<T>::popHead()
{
	T el = head->data;
	Node<T> *tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}

template<class T>
T List<T>::popTail()
{
	// TO DO!
	Node<T> *tmp = tail;

	if (tail == head)
	{
		tail = head = 0;
	}
	else
	{

		tail->next = NULL;
	}
	delete tmp;
	return NULL;
}

template<class T>
bool List<T>::search(T el)
{
	// TO DO! (Function to return True or False depending if a character is in the list.
	bool b1 = true;
	bool b2 = false;
	Node<T>* cur;
	cur = head;

	while (cur != NULL) {
		if (cur->data == el) {
			return b1;
		}
		
		cur = cur->next;
	    
	}
	if (cur->data != el) {
		return b2;
	}

	
}

template<class T>
void List<T>::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node<T> *tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}